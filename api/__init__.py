from config import config
from flask import Flask
from flask_restplus import Api
from flask_sqlalchemy import SQLAlchemy
from logger import logger

db = SQLAlchemy()
api = Api(doc='/doc/')

def create_api(config_name):
	app = Flask(__name__)
	try:
		init_config = config[config_name]()
	except KeyError as e:
		logger.exception(e)
		raise
	except Exception as e:
		logger.exception(e)
		# For unforseen exceptions
		raise

	print('Running in {} Mode'.format(init_config))
	config_object = config.get(config_name)

	app.config.from_object(config_object)
	config_object.init_app(app)

	db.init_app(app)

	from api.controllers import billing_unit_api as ns1
	from api.controllers import renew_frequency_api as ns2

	api.add_namespace(ns1, path='/billing-unit')
	api.add_namespace(ns2, path='/renew-frequency')

	api.init_app(app)

	return app
