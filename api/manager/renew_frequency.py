from logger import logger
from api.models import RenewFrequency

class RenewFreqManager(object):
    def create(self, name, role_id):
        response = {}
        if not role_id:
            response['success'] = False
            response['message'] =  "Unauthorized"
            return response, 403
        try:
            if int(role_id) < 5:
                response['success'] = False
                response['message'] =  "Unauthorized"
                return response, 403
        except ValueError as e:
            logger.exception(e)
            response['success'] = False
            response['message'] =  "Invalid request"
            return response, 400

        exists = RenewFrequency.find_renew_frequency(name=name)

        if exists:
            response['success'] = False
            response['message'] = "Renew frequency {} already exists".format(name)
            return response, 409

        new_renew_frequency = RenewFrequency.create(name)
        if new_renew_frequency:
            response['success'] = True
            response['data'] = new_renew_frequency
            return response, 200

        response['success'] = False
        response['message'] = "Error creating renew frequency {}.".format(name)
        return response, 400
    
    def edit(self, name, role_id, renew_frequency_id):
        response = {}

        if not role_id:
            response['success'] = False
            response['message'] = "Unauthorized"
            return response, 403
        try:
            if int(role_id) < 5:
                response['success'] = False
                response['message'] = "Unauthorized"
                return response, 403 
        except Exception as e:
            logger.exception(e)
            response['success'] = False
            response['message'] = "Invalid request"
            return response, 400  
        
        renew_frequency = RenewFrequency.find_renew_frequency(id=renew_frequency_id)
        if renew_frequency:
            data = renew_frequency.edit(name=name)
            response['data'] = data
            response['success'] = True
            return response, 200
        else: 
            response['message'] = "Renew frequency not found"
            response['success'] = True
            return response, 404

        response['success'] = False
        response['message'] = "Error in editing renew frequency"
        return response, 400

    @staticmethod
    def view_all(role_id):
        response = {}
        if not role_id:
            response['success'] = False
            response['message'] =  "Unauthorized"
            return response, 403
        try:
            if int(role_id) < 5:
                response['success'] = False
                response['message'] =  "Unauthorized"
                return response, 403
        except ValueError as e:
            logger.exception(e)
            response['success'] = False
            response['message'] =  "Invalid request"
            return response, 400

        data = RenewFrequency.view_all()
        response['success'] = True
        response['data'] = data
        return response, 200
    
    @staticmethod
    def view_one(role_id, renew_frequency_id):
        response = {}
        if not role_id:
            response['success'] = False
            response['message'] =  "Unauthorized"
            return response, 403
        try:
            if int(role_id) < 5:
                response['success'] = False
                response['message'] =  "Unauthorized"
                return response, 403
        except ValueError as e:
            logger.exception(e)
            response['success'] = False
            response['message'] =  "Invalid request"
            return response, 400

        renew_frequency = RenewFrequency.find_renew_frequency(id=renew_frequency_id)
        if renew_frequency:
            response['success'] = True
            response['data'] = renew_frequency.to_dict()
            return response, 200
        else:
            response['success'] = False
            response['message'] =  "Renew frequency not found"
            return response, 404
    
    @staticmethod
    def delete_renew_frequency(role_id, renew_frequency_id):
        response = {}
        if not role_id:
            response['success'] = False
            response['message'] =  "Unauthorized"
            return response, 403
        try:
            if int(role_id) < 5:
                response['success'] = False
                response['message'] =  "Unauthorized"
                return response, 403
        except ValueError as e:
            logger.exception(e)
            response['success'] = False
            response['message'] =  "Invalid request"
            return response, 400

        renew_frequency = RenewFrequency.find_renew_frequency(id=renew_frequency_id)
        if renew_frequency:
            try:
                renew_frequency.delete_renew_frequency()
                response['success'] = True
                response['data'] = renew_frequency.to_dict()
                return response, 200
            except Exception as e:
                logger.exception(e)
                response['success'] = False
                response['message'] =  "Error deleting renew frequency"
        else:
            response['success'] = False
            response['message'] =  "Renew frequency not found"
            return response, 404


        