from logger import logger
from api.models import BillingUnit

class BillingUnitManager(object):
	def create(self, name, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] =  "Unauthorized"
			return response, 403
		try:
			if int(role_id) < 5:
				response['success'] = False
				response['message'] =  "Unauthorized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] =  "Invalid request"
			return response, 400

		exists = BillingUnit.find_billing_unit(name=name)

		if exists:
			response['success'] = False
			response['message'] = "Billing unit {} already exists".format(name)
			return response, 409

		new_billing_unit = BillingUnit.create(name)
		if new_billing_unit:
			response['success'] = True
			response['data'] = new_billing_unit
			return response, 200

		response['success'] = False
		response['message'] = "Error creating billing unit {}.".format(name)
		return response, 400

	def edit(self, name, role_id, billing_unit_id):
		response = {}

		if not role_id:
			response['success'] = False
			response['message'] = "Unauthorized"
			return response, 403
		try:
			if int(role_id) < 5:
				response['success'] = False
				response['message'] = "Unauthorized"
				return response, 403
		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Invalid request"
			return response, 400

		billing_unit = BillingUnit.find_billing_unit(id=billing_unit_id)
		if billing_unit:
			data = billing_unit.edit(name=name)
			response['data'] = data
			response['success'] = True
			return response, 200
		else:
			response['message'] = "Billing unit not found"
			response['success'] = True
			return response, 404

		response['success'] = False
		response['message'] = "Error in editing billing unit"
		return response, 400

	@staticmethod
	def view_all(role_id):
		response = {}
		logger.info(f"\n\n\n{role_id}")
		logger.info(f"\n\n\n{type(role_id)}")
		if not role_id:
			response['success'] = False
			response['message'] =  "Unauthorized"
			return response, 403
		# try:
		#     if int(role_id) < 5:
		#         response['success'] = False
		#         response['message'] =  "Unauthorized"
		#         return response, 403
		# except ValueError as e:
		#     logger.exception(e)
		#     response['success'] = False
		#     response['message'] =  "Invalid request"
		#     return response, 400

		data = BillingUnit.view_all()
		response['success'] = True
		response['data'] = data
		return response, 200

	@staticmethod
	def view_one(role_id, billing_unit_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] =  "Unauthorized"
			return response, 403
		# try:
		#     if int(role_id) < 5:
		#         response['success'] = False
		#         response['message'] =  "Unauthorized"
		#         return response, 403
		# except ValueError as e:
		#     logger.exception(e)
		#     response['success'] = False
		#     response['message'] =  "Invalid request"
		#     return response, 400

		billing_unit = BillingUnit.find_billing_unit(id=billing_unit_id)
		if billing_unit:
			response['success'] = True
			response['data'] = billing_unit.to_dict()
			return response, 200
		else:
			response['success'] = False
			response['message'] =  "Billing unit not found"
			return response, 404

	@staticmethod
	def delete_billing_unit(role_id, billing_unit_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] =  "Unauthorized"
			return response, 403
		try:
			if int(role_id) < 5:
				response['success'] = False
				response['message'] =  "Unauthorized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] =  "Invalid request"
			return response, 400

		billing_unit = BillingUnit.find_billing_unit(id=billing_unit_id)
		if billing_unit:
			try:
				billing_unit.delete_billing_unit()
				response['success'] = True
				response['data'] = billing_unit.to_dict()
				return response, 200
			except Exception as e:
				logger.exception(e)
				response['success'] = False
				response['message'] =  "Error deleting billing unit"
		else:
			response['success'] = False
			response['message'] =  "Billing unit not found"
			return response, 404


