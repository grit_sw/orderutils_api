from api import api
from api.manager import BillingUnitManager
from api.schema import BillingUnitSchema
from flask import request
from flask_restplus import Namespace, Resource, fields
from logger import logger
from marshmallow import ValidationError

billing_unit_api = Namespace(
	'billing_unit', description="Api for managing billing units")

billing_unit = billing_unit_api.model('BillingUnit', {
	'name': fields.String(required=True, description="The name of the billing unit")
})

@billing_unit_api.route("/new/")
class NewBillingUnit(Resource):
	"""
		Api to create billing units
	"""

	@billing_unit_api.expect(billing_unit)
	def post(self):
		"""
			Api to create a billing unit
			@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = data or api.payload
		schema = BillingUnitSchema(strict=True)

		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = BillingUnitManager()
		role_id = request.cookies.get("role_id")
		new_payload['role_id'] = role_id
		resp, code = manager.create(**new_payload)
		return resp, code

@billing_unit_api.route("/edit/<string:billing_unit_id>/")
class EditBillingUnit(Resource):
	"""
		Api for editing a billing unit
	"""
	@billing_unit_api.expect(billing_unit)
	def post(self, billing_unit_id):
		"""
			Api to edit a billing unit
			@returns: response and status code
		"""

		data = request.values.to_dict()
		payload = data or api.payload
		schema = BillingUnitSchema(strict=True)

		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = BillingUnitManager()
		role_id = request.cookies.get("role_id")
		new_payload['role_id'] = role_id
		new_payload['billing_unit_id'] = billing_unit_id
		resp, code = manager.edit(**new_payload)
		return resp, code

@billing_unit_api.route("/one/<string:billing_unit_id>/")
class ViewABillingUnit(Resource):
	"""
		Api to get one billing unit
	"""

	def get(self, billing_unit_id):
		"""
			Api to get one billing unit
			@returns: response and status code
		"""

		role_id = request.cookies.get("role_id")
		manager = BillingUnitManager()
		resp, code = manager.view_one(role_id, billing_unit_id)
		return resp, code

@billing_unit_api.route("/all/")
class ViewAllBillingUnits(Resource):
	"""
		Api to get all billing units
	"""

	def get(self):
		"""
			Api to get one billing unit
			@returns: response and status code
		"""

		role_id = request.cookies.get("role_id")
		manager = BillingUnitManager()
		resp, code = manager.view_all(role_id)
		return resp, code

@billing_unit_api.route("/delete-billing-unit/<string:billing_unit_id>/")
class DeleteBillingUnit(Resource):
	"""
		Api to delete a billing unit
	"""
	def delete(self, billing_unit_id):
		"""
			Api to delete a billing unit
		"""
		role_id = request.cookies.get("role_id")
		manager = BillingUnitManager()
		resp, code = manager.delete_billing_unit(role_id, billing_unit_id)
		return resp, code
