from api import api
from api.manager import RenewFreqManager
from api.schema import RenewFreqSchema
from flask import request
from flask_restplus import Namespace, Resource, fields
from logger import logger
from marshmallow import ValidationError

renew_frequency_api = Namespace(
	'renew_frequency', description="Api for managing billing units")

renew_frequency = renew_frequency_api.model('RenewFreq', {
	'name': fields.String(required=True, description="The name of the billing unit")
})

@renew_frequency_api.route("/new/")
class NewRenewFreq(Resource):
	"""
		Api to create billing units
	"""

	@renew_frequency_api.expect(renew_frequency)
	def post(self):
		"""
			Api to create a billing unit
			@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = data or api.payload
		schema = RenewFreqSchema(strict=True)

		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = RenewFreqManager()
		role_id = request.cookies.get("role_id")
		new_payload['role_id'] = role_id
		resp, code = manager.create(**new_payload)
		return resp, code

@renew_frequency_api.route("/edit/<string:renew_frequency_id>/")
class EditRenewFreq(Resource):
	"""
		Api for editing a billing unit
	"""
	@renew_frequency_api.expect(renew_frequency)
	def post(self, renew_frequency_id):
		"""
			Api to edit a billing unit
			@returns: response and status code
		"""

		data = request.values.to_dict()
		payload = data or api.payload
		schema = RenewFreqSchema(strict=True)

		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = RenewFreqManager()
		role_id = request.cookies.get("role_id")
		new_payload['role_id'] = role_id
		new_payload['renew_frequency_id'] = renew_frequency_id
		resp, code = manager.edit(**new_payload)
		return resp, code

@renew_frequency_api.route("/view-billing-unit/<string:renew_frequency_id>/")
class ViewARenewFreq(Resource):
	"""
		Api to get one billing unit
	"""

	def get(self, renew_frequency_id):
		"""
			Api to get one billing unit
			@returns: response and status code
		"""

		role_id = request.cookies.get("role_id")
		manager = RenewFreqManager()
		resp, code = manager.view_one(role_id, renew_frequency_id)
		return resp, code

@renew_frequency_api.route("/view-all/")
class ViewAllRenewFreq(Resource):
	"""
		Api to get all billing units
	"""

	def get(self):
		"""
			Api to get one billing unit
			@returns: response and status code
		"""

		role_id = request.cookies.get("role_id")
		manager = RenewFreqManager()
		resp, code = manager.view_all(role_id)
		return resp, code

@renew_frequency_api.route("/delete-billing-unit/<string:renew_frequency_id>/")
class DeleteRenewFreq(Resource):
	"""
		Api to delete a billing unit
	"""
	def delete(self, renew_frequency_id):
		"""
			Api to delete a billing unit
		"""
		role_id = request.cookies.get("role_id")
		manager = RenewFreqManager()
		resp, code = manager.delete_renew_frequency(role_id, renew_frequency_id)
		return resp, code
