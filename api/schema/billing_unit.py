from marshmallow import Schema, fields, post_load, validates, ValidationError
from collections import namedtuple

NewBillingUnit = namedtuple('NewBillingUnit', [
    'name'
])

class BillingUnitSchema(Schema):
    name = fields.String(required=True)

    @post_load
    def new_billing_unit(self,data):
        return NewBillingUnit(**data)
    
    @validates('name')
    def validate_name(self, value):
        if len(value) <= 0:
            raise ValidationError("Billing unit name required")

    def __repr__(self):
        return "BillingUnitSchema <{}>".format(self.name)