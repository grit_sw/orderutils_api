from uuid import uuid4

import arrow
from api import db
from logger import logger


class Base(db.Model):
	__abstract__ = True
	id = db.Column(db.Integer, autoincrement=True, primary_key=True)
	date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
	date_modified = db.Column(
		db.DateTime,
		default=db.func.current_timestamp(),
		onupdate=db.func.current_timestamp()
	)

class BillingUnit(Base):
	id = db.Column(db.Integer, autoincrement=True, primary_key=True)
	name = db.Column(db.String(100), nullable=False)
	billing_unit_id = db.Column(db.String(100), unique=True, nullable=False)

	def __init__(self, name):
		self.name = name.title()
		self.billing_unit_id = str(uuid4())

	@staticmethod
	def create(billing_unit_name):
		new = BillingUnit(name=billing_unit_name)

		db.session.add(new)
		db.session.commit()
		db.session.refresh(new)
		return new.to_dict()

	def edit(self, name):
		self.name = name.title()
		try:
			db.session.add(self)
			db.session.commit()
			db.session.refresh(self)
		except Exception as e:
			db.session.rollback()
			logger.exception(e)

		return self.to_dict()

	@staticmethod
	def find_billing_unit(id=None, name=None):
		if id:
			billing_unit = BillingUnit.query.filter_by(billing_unit_id=id).first()
			return billing_unit
		elif name:
			name = name.title()
			billing_unit = BillingUnit.query.filter_by(name=name).first()
			return billing_unit
		else:
			raise Exception("Name or ID required")

	@staticmethod
	def setup():
		units = ["Energy N/Wh", "Time N/day"]
		for unit in units:
			if not BillingUnit.find_billing_unit(name=unit):
				BillingUnit.create(unit)

	def delete_billing_unit(self):
		try:
			db.session.delete(self)
			db.session.commit()
		except Exception as e:
			db.session.rollback()

	@staticmethod
	def view_all():
		all_billing_units = BillingUnit.query.order_by(BillingUnit.name.asc()).all()
		return [billing_unit.to_dict() for billing_unit in all_billing_units]

	def to_dict(self):
		unit_dict = {
			'billing_unit_name': self.name,
			'billing_unit_id': self.billing_unit_id
		}
		return unit_dict

class RenewFrequency(Base):
	id = db.Column(db.Integer, autoincrement=True, primary_key=True)
	name = db.Column(db.String(100), nullable=False)
	renew_frequency_id = db.Column(db.String(100), unique=True, nullable=False)

	def __init__(self, name):
		self.name = name.title()
		self.renew_frequency_id = str(uuid4())

	@staticmethod
	def create(renew_frequency_name):
		new = RenewFrequency(name=renew_frequency_name)

		db.session.add(new)
		db.session.commit()
		db.session.refresh(new)
		return new.to_dict()

	def edit(self, name):
		self.name = name.title()
		try:
			db.session.add(self)
			db.session.commit()
			db.session.refresh(self)
		except Exception as e:
			db.session.rollback()
			logger.exception(e)

		return self.to_dict()

	@staticmethod
	def setup():
		frequencies = ["Energy N/Wh", "Time N/day"]
		for frequency in frequencies:
			if not RenewFrequency.find_renew_frequency(name=frequency):
				RenewFrequency.create(frequency)

	@staticmethod
	def find_renew_frequency(id=None, name=None):
		if id:
			renew_frequency = RenewFrequency.query.filter_by(renew_frequency_id=id).first()
			return renew_frequency
		elif name:
			renew_frequency = RenewFrequency.query.filter_by(name=name.title()).first()
			return renew_frequency
		else:
			raise Exception("Name or ID required")

	def delete_renew_frequency(self):
		try:
			db.session.delete(self)
			db.session.commit()
		except Exception as e:
			db.session.rollback()

	@staticmethod
	def view_all():
		all_renew_frequencies = RenewFrequency.query.order_by(RenewFrequency.name.asc()).all()
		return [renew_frequency.to_dict() for renew_frequency in all_renew_frequencies]

	def to_dict(self):
		freq_dict = {
			'renew_frequency_name': self.name,
			'renew_frequency_id': self.renew_frequency_id
		}
		return freq_dict
