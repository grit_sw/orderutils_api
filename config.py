import os
from ast import literal_eval

class Config(object):
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.urandom(20)
    MIGRATE_DB = os.environ['MIGRATE_DB']

    BOOTSTRAP_SERVERS = os.environ['BOOTSTRAP_SERVERS']
    SCHEMA_REGISTRY_URI = os.environ['SCHEMA_REGISTRY_URI']
    NEW_ORDERUTIL_TOPIC = os.environ['NEW_ORDERUTIL_TOPIC']
    TOPICS = literal_eval(os.environ['TOPICS'])
    SCHEMA_MAPPING = literal_eval(os.environ['SCHEMA_MAPPING'])
    TOPIC_SCHEMA_MAPPING = dict(zip(TOPICS, SCHEMA_MAPPING))

    @classmethod   
    def init_app(cls, app):
        # from api.producer import KafkaProducer
        # app.kafka_producer = KafkaProducer(
        #     bootstrap_servers=cls.BOOTSRAP_SERVERS,
        #     schema_registry_uri=cls.SCHEMA_REGISTRY_UI,
        #     topic_schema_mapping=cls.TOPIC_SCHEMA_MAPPING
        # )
        # app.kafka_producer.check_registry()
        pass

class Development(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    ORDERUTILS_URL = 'http://localhost:5050/orderutils/orderutils/'

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)

    @staticmethod
    def init_app(app, *args):
        pass

class Testing(Config):
    """TODO"""
    pass

class Staging(Config):
    """TODO"""
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)

class Production(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)

config = {
    'development': Development,
    'testing': Testing,
    'Production': Production,
    'Staging': Staging,

    'default': Development
}