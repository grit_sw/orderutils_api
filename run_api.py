# Test Server.
from os import environ

from api import create_api, db
from arrow import now
# from flask_migrate import Migrate, upgrade
from flask import request
from logger import logger

if environ.get('FLASK_ENV') is None:
	print('FLASK_ENV not set')
app = create_api(environ.get('FLASK_ENV') or 'development')

# migrate = Migrate(app, db)
# to_migrate = bool(app.config.get('MIGRATE_DB'))

@app.before_first_request
def setup_billing_units():
	from api.models import BillingUnit, RenewFrequency
	BillingUnit.setup()
	RenewFrequency.setup()
	from pprint import pprint
	pprint(BillingUnit.view_all())
	pprint(RenewFrequency.view_all())

@app.after_request
def log_info(response):
	try:
		log_data = {
			'connection': request.headers.get('Connection'),
			'ip_address': request.remote_addr,
			'browser_name': request.user_agent.browser,
			'user_device': request.user_agent.platform,
			'referrer': request.referrer,
			'request_url': request.url,
			'host_url': request.host_url,
			'status_code': response.status_code,
			'date': str(now('Africa/Lagos')),
			'location': response.location
		}
		logger.info('orderutils_api_logs: logs: {}'.format(log_data))

	except Exception as e:
		logger.exception('orderutils_api_logs: {}'.format(e))
	return response

with app.app_context():
	db.create_all()

# if to_migrate is True:
#     with app.app_context():
#         try:
#             upgrade()
#         except Exception as e:
#             logger.warn(e)
#             db.session.rollback()
